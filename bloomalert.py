import model_api
from sensetdp.parsers import PandasObservationParser
from sensetdp.models import Observation, UnivariateResult
import json
import pandas as pd
import os
import numpy as np
from datetime import datetime
import pytz
import sys

app = model_api.Application()
host = sys.argv[3]

measures = []
measures.append({"name":"bga",
                 "property":"http://data.sense-t.org.au/registry/def/sop/Phycocyanin",
                 "unit":"http://registry.it.csiro.au/def/environment/unit/MicrogramsPerLitre"})

#measures.append({"name":"chla",
#                 "property":"http://registry.it.csiro.au/def/environment/property/chlorophyll_a_concentration",
#                 "unit":"http://registry.it.csiro.au/def/environment/unit/MicrogramsPerLitre"})
measures.append({"name":"ratio",
                 "property":"http://data.sense-t.org.au/registry/def/sop/Phycocyanin",
                 "unit":"http://registry.it.csiro.au/def/qudt/1.1/qudt-unit/Percent"})



stream_template = {
    "id": "bloomalert.id",
    "resulttype": "scalarvalue",
    "organisationid": "utas",
    "groupids": [
        "bloomalert_project.dev"
    ],
    "samplePeriod": "PT15M",
    "reportingPeriod": "PT15M",
     "streamMetadata":{
        "type": ".ScalarStreamMetaData",
        "interpolationType": "http://www.opengis.net/def/waterml/2.0/interpolationType/Continuous",
        "cummulative": "false",
        "observedProperty": "http://registry.it.csiro.au/def/environment/property/",
        "unitOfMeasure": "http://registry.it.csiro.au/def/qudt/1.1/qudt-unit/"
        
    }
}

eastern = pytz.timezone('Australia/Hobart')

def createOutputStreams(context):
    output_stream_ids = context.ports['outputs'].stream_ids
    print measures
    for s in output_stream_ids:
        name = "bga"
        if s.find("ratio") >= 0:
            name = "ratio" 
        m = filter(lambda m: m['name'] == name, measures)
        print s
        print m
        print "\n"
        #print s + " " + obs + " " + unit
        template = stream_template
        template['streamMetadata']['observedProperty'] = m[0]['property']
        template['streamMetadata']['unitOfMeasure'] = m[0]['unit']
        print template['streamMetadata']
        cs = context.sc_api.create_stream(id=s, groupids=template["groupids"],resulttype=template["resulttype"],
                                          organisationid=template["organisationid"], reportingPeriod=template["reportingPeriod"],
                                          samplePeriod=template["samplePeriod"], streamMetadata=template["streamMetadata"])


def mapInputsToSpecies(instreams, species):
    species = []
    for s in instreams:
        if s.find("phyco") >= 0: 
            species.append({s:"phycocyanin"})
        if s.find("chlor") >= 0: 
            species.append({s:"chlorophyll"})
    print species
    
def getAnalysisSensor(output):
    sensor='SENSOR'
    rdi = output.rfind(".") 
    if (rdi < 0):
        print "ERROR : No sensor found, expect structure platform.sensor in " + output 
        return "sensor"
    rdi = rdi +  1
    print output + " -> " + output[rdi:]
    sensor = output[rdi:]
    return sensor


def nighttime(dt):
    if dt.dst():
        return dt.hour not in [20,21,22,23,0,1,2,3,4,5]
    return dt.hour not in [18,19,20,21,22,23,0,1,2,3,4,5,6]

@app.model('bloomalert')
def bloomalert(context):
    modelapp = "bloomalert"
    input_stream_ids = context.ports['inputs'].stream_ids
    output_stream_ids = context.ports['outputs'].stream_ids
    params_doc = getattr(context.ports.get("params"), 'document', None) or '{}'
    params = json.loads(params_doc)

    species = []
    mapInputsToSpecies(input_stream_ids, species)
    
    outsensors = []
    for s in output_stream_ids:
        outsensors.append(getAnalysisSensor(s))
    print outsensors

    
    # Obtain observation data from `inputs` streams.
    context.update(message='Loading data...')
    limit = 10000

    while True:
        data = context.sc_api.get_observations(streamid=','.join(input_stream_ids), media='csv', limit=limit, start=params['start'], end=params['end'], parser=PandasObservationParser())
        if len(data) < limit:
            break
        limit *= 10

    
    index = data.index
    # localise
    data.index = index.tz_localize(pytz.utc).tz_convert(eastern)
    data.to_csv('data.csv')

    
    context.update(message='Producing resampled output ...')
    # Compute resampled mean for analysis streams ...

    cleandata = pd.DataFrame()
    for instream in input_stream_ids:
        s = pd.Series(data[instream])
        s[s < 0] = np.nan
        s.fillna(method='ffill', inplace=True)
        s.fillna(method='bfill', inplace=True)
        s.to_csv(instream+'.csv', float_format="%0.4f")
        cleandata[instream] = s
        si = s.index
        ns = s[s.index.map(lambda x: nighttime(x))]     
        ns = ns.reindex(si)
        cleandata[instream + ".night"]  = ns
        ns.to_csv(instream+'.night.csv', float_format="%0.4f")

        
    means = pd.DataFrame()     
    means['bga-pc-24h'] = cleandata['bloomalert.craigbourne.exo2.blue-green_algae_phycocyanin_concentration_ug_per_L'].resample(rule='24H').mean()  
    means['bga-pc-night'] = cleandata['bloomalert.craigbourne.exo2.blue-green_algae_phycocyanin_concentration_ug_per_L.night'].resample(rule='24H').mean()  
  
    means.to_csv('means.csv')


    # set path - context not setting env properly 20161025
    if not hasattr(context, "mypath"):
        context.mypath = "/opt/model/"

    analysis = pd.DataFrame()    
    #analysis[output_stream_ids[0]] = pd.DataFrame(np.random.randn(100, len(output_stream_ids)), columns=output_stream_ids)
    
    analysis[outsensors[0]] = means[outsensors[0]]   
    index = analysis.index
    analysis.index = index.tz_convert(pytz.utc)


    
    context.update(message='Writing output streams ...')
    createOutputStreams(context)
    return        
    output = Observation()
    output.results = [UnivariateResult(t=t.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),v=v) for t,v in zip(analysis.index, analysis.ix[:,outsensors[0]].astype('float'))]
    if not output.results:
        raise ValueError("No output data for " + output_stream_ids[0])
    context.sc_api.create_observations(output, streamid=output_stream_ids[0])


                            
if not app.run():
    from sensetdp.api import API
    from sensetdp.auth import HTTPBasicAuth
    from as_client import Client
    import sys

    class Context(object):
        pass

    class Port(object):
        def __init__(self, type_, direction):
            self.type = type_
            self.direction = direction

    class StreamPort(Port):
        def __init__(self, direction, stream_id):
            self.stream_id = stream_id
            super(StreamPort, self).__init__('stream', direction)

    class MultistreamPort(Port):
        def __init__(self, direction, stream_ids):
            self.stream_ids = stream_ids
            super(MultistreamPort, self).__init__('multistream', direction)

    class DocumentPort(Port):
        def __init__(self, direction, document):
            self.document = document
            super(DocumentPort, self).__init__('document', direction)

    context = Context()
    context.model_id = 'bloomalert'
    context.ports = {
        'inputs': MultistreamPort('input', ['bloomalert.craigbourne.exo2.blue-green_algae_phycocyanin_concentration_ug_per_L',
                                            'bloomalert.craigbourne.exo2.chlorophyll_a_concentration_ug_per_L']),
        'params': DocumentPort('input', '{"start": "2015-11-18T00:00:00.000Z", "end" : "2016-12-31T00:00:00.000Z", "samplerule" : "12H"}'),
        'outputs': MultistreamPort('output', [
            "bloomalert.craigbourne.analysis.bga-pc-24h",
            "bloomalert.craigbourne.analysis.bga-pc-night",
            "bloomalert.craigbourne.analysis.bga-pc-chla-ratio-24h",
            "bloomalert.craigbourne.analysis.bga-pc-chla-ratio-night",
            "bloomalert.craigbourne.analysis.bga-pc-24h-corrected",
            "bloomalert.craigbourne.analysis.bga-pc-night-corrected",
            "bloomalert.craigbourne.analysis.bga-pc-chla-ratio-24h-corrected",
            "bloomalert.craigbourne.analysis.bga-pc-chla-ratio-night-corrected",
            "bloomalert.craigbourne.analysis.bga-pc-chla-ratio-24h-percent",
            "bloomalert.craigbourne.analysis.bga-pc-chla-ratio-night-perccent",
            "bloomalert.craigbourne.analysis.bga-pc-alert"
            
                ])
    }

    context.sc_api = API(HTTPBasicAuth(sys.argv[1], sys.argv[2]), host)
    context.as_api = Client('https://'+host+'/api/analysis/v2', (sys.argv[1], sys.argv[2]))
    context.update = lambda progress=None, message=None: None
    context.terminate = lambda: None
    context.mypath = "./"
    context.noexec = False
    bloomalert(context)

# README #

## Run Model Locally ##

* An analysis services model with the appropriate environment detection can be run from user machine.
* The code within the model that detects the local run environment is ...

```
#!python

if __name__ == "__main__":                                
    if not app.run():
        import sys
        if len(sys.argv) != 5:
            print sys.argv
            print "usage: python bloomalert.py <user> <pwd <host> <site>"
            assert len(sys.argv) == 5
```

* To run it locally ...

```
#!bash

 python bloomalert.py chris.boucher@utas.edu.au <pwd> data.sense-t.org.au milford

```


# Analysis Services #

* See 

## Upload Model ###

* manifest.json - prescribes model name and requirements for workflow (inputs, outputs)
* bloomalert.py - Python model


```
#!bash

tar -czf bloomalert.tar.gz manifest.json bloomalert.py
```
![Screenshot from 2017-05-26 09:04:46.png](https://bitbucket.org/repo/7d8kRy/images/3701370469-Screenshot%20from%202017-05-26%2009:04:46.png)

## Upload Workflow ##

* workflow.json - specifies streams and parameters for a particular model run

![Screenshot from 2017-05-26 12:02:20.png](https://bitbucket.org/repo/7d8kRy/images/59124593-Screenshot%20from%202017-05-26%2012:02:20.png)


## Run Model / Workflow ##